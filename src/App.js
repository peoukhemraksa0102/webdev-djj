
import Payment from '../src/components/payment.js'
import Login from '../src/components/login.js'
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'
import Home from './components/home'
import Price from './components/priceEstimation'
import Signup from './components/signup.js'
import About from './components/aboutPage.js'
import ContactUs from './components/contactUs.js'
import Faq from './components/faq.js'
import Navbar from "./components/navbar"


function App() {
  return (
    <div className="App">
      <Router>
        <Navbar></Navbar>
        <Switch>
          <Route exact path='/'>
            <Home></Home>
          </Route>
          <Route exact path='/signup' component={Signup}>
          </Route>
          <Route exact path="/contactUs">
            <ContactUs></ContactUs>
          </Route>
          <Route exact path="/payment">
            <Payment></Payment>
          </Route>
          <Route exact path="/login">
            <Login></Login>
          </Route>
          <Route exact path="/priceEstimation">
            <Price></Price>
          </Route>
          <Route exact path="/aboutPage">
            <About />
          </Route>
          <Route exact path="/faq">
            <Faq />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
