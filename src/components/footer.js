import React from 'react';
import "../css/footer.css"
const Footer = () => {
    return (
        <div className="footer">
        <div className="Footer-container">
        <div className="Footerrow">
          <div className="col-md-8 col-sm-6 col-xs-12">
            <p id="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
         <a href="#"> DJJ</a>.
            </p>
          </div>

          <div className="col-md-4 col-sm-6 col-xs-12">
            <ul className="social-icons">
              <li><a className="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a className="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a className="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
              <li><a className="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
        <div className="footer2">
        <p id="backtotop"><a href="#">Back to top</a></p>
         </div>
        </div>
    );
}

export default Footer;
