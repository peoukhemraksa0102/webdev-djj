import React from 'react'
import "../css/whychoseus.css"


function WhyChoseus() {
    return (

        <div className="third-banner">
            <h2>WHY CHOOOSE DJJ ?</h2>
            <div className="feature-container">
            <div className="feature">
                <img src="wallet.svg" alt="Wallet"/>
                <div className="feature-detail">
                    <p>Reasonable</p>
                    <p>Price</p>
                </div>
            </div>
            <div className="feature">
                <img src="envelope.svg"/>
                <div className="feature-detail">
                    <p>Quick and</p>
                    <p>Reliable</p>
                </div>
            </div>
            <div className="feature">
                <img src="box.svg" alt=""/>
                <div className="feature-detail">
                    <p>Mask</p>
                    <p>On</p>
                </div>
            </div>
            <div className="feature">
                <img src="lan.svg" />
                <div className="feature-detail">
                    <p>Safe</p>
                    <p>and Sound</p>                   
                </div>
            </div>
            </div>
            
        </div>
    )
}


export default WhyChoseus; 