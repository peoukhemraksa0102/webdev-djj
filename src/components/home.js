import { useHistory } from 'react-router-dom'
import "../css/home.css"
import WhyChoseus from "./whyChoseus"
import Footer from "./footer"
import Navbar from "./navbar"
export default function Home() {
  const history = useHistory()
  const redirectPriceEstimation = () => {
    history.push('/priceEstimation')
  }
  return (
    <div className="top-banner">
     
      <div className="containerZ">
        <div className="flex-item-1">
          <img className="deliveryImg" src="6447.jpg"></img>
        </div>
        <div className="flex-item-2">
          <h1>Pick the plan that works for YOU!</h1>
          <div className="texxt">Input your product dimension and get the estimation cost.</div>
          <button className="buttonPriceEstimation" onClick={redirectPriceEstimation}>Go to Shipment</button>    
        </div>
        
      <div>
      
      </div>
      </div>

      <div>
        <WhyChoseus></WhyChoseus>
      </div>
      <div className="carouselContainer">
        <div id="myCarousel" className="carousel slide" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner">
            <div className="item active">
              <img src="wing.jpg"/>
              <div className="carousel-caption">
                <h3>Boeng Keng Kang</h3>
                <p>BKK is always so much fun!</p>
              </div>
            </div>

            <div className="item">
              <img src="pic3.jpg"/>
              <div className="carousel-caption">
                <h3>Olympic</h3>
                <p>Thank you, Olympic!</p>
              </div>
            </div>

            <div className="item">
              <img src="pic5.jpg"/>
              <div className="carousel-caption">
                <h3>Toul Kork</h3>
                <p>What a beautiful district</p>
              </div>
            </div>

          </div>

          <a className="left carousel-control" href="#myCarousel" data-slide="prev">
            <span className="glyphicon glyphicon-chevron-left"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="right carousel-control" href="#myCarousel" data-slide="next">
            <span className="glyphicon glyphicon-chevron-right"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
      {/* <button onClick={redirectLogin}>To Login Page</button> */}
      
      {/* <button onClick={redirectSignup}>To Signup Form </button> */}
      {/* <button onClick={redirectAboutPage}>To About Page</button>
      <button onClick={redirectPayment}>To Payment</button> */}
      <div>
        <Footer/>
      </div>
      
    </div>
  )
}