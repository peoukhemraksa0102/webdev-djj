import React from 'react'
import { useHistory } from 'react-router-dom'
export default function Navbar() {
    const history = useHistory()
  const redirectLogin = () => {
    history.push('/login')
  }
 
  const redirectSignup = () => {
    history.push('/signup')
  }
  const redirectAboutPage = () => {
    history.push('/aboutPage')
  }
  const redirectContactPage = () => {
    history.push('/contactUs')
  }
  const redirectPayment = () => {
    history.push('/payment')
  }

  const redirectFaq = () => {
    history.push('/faq')
  }

  const redirectHome = () =>
  {
    history.push('/')
  }
    return (

             <nav className="navbar">
        <div className="logo">
          <a>DJJ</a>
        </div>
        <ul className="navbar-nav" >
          <li><a href="#" onClick={redirectHome}>Home</a></li>
          <li><a href="#" onClick={redirectFaq}>FAQ</a></li>
          <li><a href="#" onClick={redirectAboutPage}>About</a></li>
          <li><a href="#" onClick={redirectContactPage}>Contact</a></li>
          <li><a onClick={redirectSignup} ><span id="signup">Sign Up</span></a></li>
          <li><a onClick={redirectLogin}><span id="login">Login</span></a></li>
        </ul>
      </nav>
     
    )
}
