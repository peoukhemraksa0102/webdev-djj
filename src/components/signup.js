import React, {useState,useEffect}from 'react';
import axios from "axios"
import {useHistory} from "react-router-dom"
import "../css/signup.css"


const Signup = () => {


  const history = useHistory()
  const [user, setUser] = useState({
    email: "",
    password: "",
  })



  const [allUser, setAllUser] = useState([])


  const handleChange = (key, value) => {
    setUser(prevState => ({ ...prevState, [key]: value }))
    console.log(user);
  }


  useEffect(() => {   
    const fetchUser = async()=>{
        const response = await axios.get("http://localhost:3000/user")
        setAllUser(response.data)
    }
  
    fetchUser()
},[]);


  const handleSignUp = async(e) => {
    e.preventDefault()

    allUser.allUser.push(user)

    setAllUser(allUser)

   
      try {
        const response = await axios.put("http://localhost:3000/user", allUser)
        console.log(response)
        if(response.statusText === "OK"){
          history.push("/")
        }
       
      }
      catch (e) {
        console.log(e);
      }
    
    
}

  return (
    <div>
      <h1 className="header text-center bg-dark header"> Welcome to DJJ Company </h1>
      <h3 className="body text-center"> Sign Up </h3>
      <form>
        <div className="container">
          <label>Email: </label>
          <input type="text" placeholder="Enter Email" name="username" required onChange={(e) => handleChange("email", e.target.value)}></input>
          <label>Password : </label>
          <input type="password" placeholder="Enter Password" name="password" onChange={(e) => handleChange("password", e.target.value)} required></input>
          <button type="submit" onClick={(e) => handleSignUp(e)} >Sign Up</button>
          <button type="cancel" className="cancelbtn"> Cancel</button>

        </div>
      </form>
    </div>
  );
}


export default Signup;