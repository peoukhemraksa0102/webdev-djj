import React from 'react';
import "../css/aboutPage.css"
import Footer from "./footer"

const About  = () => {
    return (
        <div>
            <main>

                    <div className="container marketing">
                        <div className="row">
                            <div className="col-lg-4">
                            <img src="https://avagroup.az/wp-content/uploads/2020/01/what_exactly_is_digital_transformation_and_its_benefits.png" className="img2" alt="" width="200" height="200"/>
                            <h2>Digitilization</h2>
                            <p>Digital technology is bringing the world together, keeping us connected, and opening up even more and completely new opportunities. It is easier and faster.Digitalization is playing a key role in our strategy. We believe it will make our business more efficient and effective, empower our people to provide ever improving service to our customers, and create new opportunities in logistics.</p>
                            </div>
                            <div className="col-lg-4">
                            <img src="about3.jpg" className="img2" alt="" width="200" height="200"/> 
                            <h2>Sustainability</h2>
                            <p>Sustainability is a cornerstone of our company and key to our long-term success. By making it our busines to be sustainable, we are investing in our future and our ability to connect people and improve lives. We hope others will follow our lead.To achieve our goals and make sound investments in the future, we actively pursue trusted and sustainable business practices wherever we operate.</p>
                            </div>
                            <div className="col-lg-4">
                            <img src="about4.jpg" className="img2" alt="" width="200" height="200"/>
                            <h2>Globalization</h2>
                            <p>Globalization – the growing connectedness of people, businesses, societies, and countries worldwide.In fact, omnipresent today. Clearly, an exceptionally positive force is at work here – one that we need to harness better. The more open, connected, and integrated societies and businesses become, the more opportunities for prosperity,transparent freedom, and stability we can create all over the world.</p>
                        </div>
                    </div>

                    <hr className="featurette-divider"/>
                        <div className="row featurette">
                            <div className="col-md-7">
                                <h2 className="featurette-heading">Kemhout Lem<span className="text-muted"></span></h2>
                                <p className="lead">The Process with Technology reach to high Successful.</p>
                                </div>
                                <div className="col-md-5">
                                <img src="picabout.jpg" className="img1" alt="" width="400" height="400"/>
                            </div>
                        </div>

                    <hr className="featurette-divider"/>
                        <div className="row featurette">
                            <div className="col-md-7 order-md-2">
                                <h2 className="featurette-heading">Morakot Vutheaya <span className="text-muted"></span></h2>
                                <p className="lead">The Resposibility is what we together want in Business.</p>
                                </div>
                                <div className="col-md-5 order-md-1">
                                    <img src="about2.jpg" className="img1" alt="" width="400" height="400"/>
                                
                            </div>
                        </div>

                    <hr className="featurette-divider"/>

                    <div className="row featurette">
                        <div className="col-md-7">
                        <h2 className="featurette-heading">Chanlak Sou <span className="text-muted"></span></h2>
                        <p className="lead">The good manner and respect are big step in Business.</p>
                        </div>
                        <div className="col-md-5">
                        <img src="picabout9.png" className="img1" alt="" width="400" height="400"/>
                        </div>
                        <hr className="featurette-divider"/>
                        <div className="row featurette">
                            <div className="col-md-7 order-md-2">
                                <h2 className="featurette-heading">Khem Raksa Peou <span className="text-muted"></span></h2>
                                <p className="lead">The Business together with Technology is what we do in Digital8.</p>
                                </div>
                                <div className="col-md-5 order-md-1">
                                    <img className="img3" src="about1.jpg" alt="" width="400" height="400"/>
                            </div>
                        </div>
                    </div>

                    <hr className="featurette-divider"/>
                </div>

                <div>
                    <Footer/>
                </div>
            </main>
        </div>
    )
}

export default About; 