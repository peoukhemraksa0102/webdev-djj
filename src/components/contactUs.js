import React from 'react';
import "../css/contactUs.css"

const ContactUs = () => {
    return (
        <div>
            <div className="contact-flex-container">
                <div className="contact-flex-item-1">
                    <h1>Contact Information</h1>
                    <div>Fill up the information and our Team will get back to you within 3 hours.</div>
                    <div className="contact-flex-container-1-text">
                        <div>
                            <i className="fa fa-phone">&nbsp;&nbsp;</i>
                            <span>+855 12-345-6789</span>
                        </div>
                        <div>
                            <i className="fa fa-facebook">&nbsp;&nbsp;</i>
                            <span>DJJ Facebook Page</span>
                        </div>
                        <div>
                            <i className="fa fa-envelope">&nbsp;&nbsp;</i>
                            <span>sale@djj.shop</span>
                        </div>
                    </div>
                </div>
                <div className="contact-flex-container-2">
                    <div className="contact-flex-container-3">
                        <div className="contact-flex-item-2">
                            <label for="fname">First name:</label>
                            <input className="contact-input" type="text" id="fname" name="fname"/><br></br><br></br>
                            <label for="mail">Mail:</label>
                            <input className="contact-input" type="email" id="email" name="email"/><br></br><br></br>
                        </div>
                        <div className="contact-flex-item-3">
                            <label for="lname">Last name:</label>
                            <input className="contact-input" type="text" id="lname" name="lname"/><br></br><br></br>
                            <label for="telephone">Phone:</label>
                            <input className="contact-input"type="text" id="telephone" name="telephone"/><br></br><br></br>
                        </div>
                    </div>
                    <div className="container-flex-container-4">
                        <div className="container-flex-item-4">
                        <form>
                            <p className="flex-container-4-text">What do you want us to improve.</p>
                            <label className="checkbox-inline">
                                <input className="contact-input" type="checkbox"/>Web Design
                            </label>
                            <label className="checkbox-inline">
                            <   input className="contact-input" type="checkbox"/>Web Development
                            </label>
                            <label className="checkbox-inline">
                                <input className="contact-input" type="checkbox"/>Logo Design
                            </label>
                            <label className="checkbox-inline">
                                <input className="contact-input" type="checkbox"/>Other
                            </label>
                            <p className="flex-container-4-text">Message: </p>
                            <textarea name="message" rows="3" cols="50"></textarea>
                            <input className="contact-input" type="submit" value="Submit"></input>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ContactUs;   