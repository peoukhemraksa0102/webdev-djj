import { Alert } from 'react-st-modal';


function AlertExample() {
    return (
      <div>
        <button
          onSubmit={async () => {
            await Alert('Alert text', 'Alert title');
          }}
        >
            Alert
        </button>
      </div>
    );
  }

export default AlertExample;