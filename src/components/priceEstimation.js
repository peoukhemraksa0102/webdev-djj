import React, { useState } from 'react';
import { useHistory } from 'react-router';
//import $ from "jquery";
import "../css/priceEstimation.css";
// import AlertExample from './confirmExample.js'
// import { Alert } from 'react-st-modal';
// import { useHistory } from 'react-router-dom'

export default function PriceEstimation () {
var driverPic;
var errorMessage;





// const PriceEstimation = () => {

  const [width, setWidth] = useState('');
  const [length, setLegth] = useState('');
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [distance, setDistance] = useState('');
  var [result, setResult] = useState(0);  
  var [totalPrice, setTotalPrice] = useState(0);
  const handleWidthChange = (e) => {
    setWidth(parseInt(e))
  }
  const handleLengthChange = (e) => {
    setLegth(parseInt(e))
  }
  const handleHeightChange = (e) => {
    setHeight(parseInt(e))
  }
  const handleWeightChange = (e) => {
    setWeight(parseInt(e))
  }
  const handleDistanceChange = (e) => {
    setDistance(parseInt(e))
  }

  // const [flip, setFlip] = useState(false);
  // const handleFlip = () => {
    
  // }

  const handleSubmit = (e) => {
    e.preventDefault();
    setResult((width + height + length + weight));
    
    // if (result > 100) {
    //   setFlip(true)
    // }
  }

  if (result > 0 && result < 100) {
    driverPic = (
      <div>
        <div className="img-container">
            <img src="motorDrive.jpg" alt="motorDriver" className="driver"weight="400" height="400"/>
            <div className="text-block">
               
            </div>
        </div>
       </div>
    )
  } else if (result >= 100 ) {
    driverPic = (
      <div>
        <div className="img-container">
          <img src="tuktukDrive.jpg" alt="tuktukDriver" className="driver" weight="400" height="400"/>
          <div className="text-block">
            
          </div>
        </div>
      </div>
    )
    } else {
      driverPic = (
        <div>
          <div className="img-container">
            <img src="pic4.jpg" alt="tuktukDriver" weight="400" height="400" className="driver"/>
            <div className="text-block">
                Your Selected Driver
            </div>
          </div>
        </div>
      )
  }

  // function AlertExample() {
  //     return (
  //       <div>
  //         <button
  //           type="submit" 
  //           className="btn btn-primary" 
  //           className="submitButton"
  //           onSubmit={async () => {
  //             await Alert(result, 'Alert Kmehout');
  //           }}
  //         >
  //           Estimate
  //         </button>
  //       </div>
  //     );
  // }
  const history = useHistory();
  const redirectPayment = () => {
  history.push('/payment')
}
  return (
    <div>
      <div className="price-flex-container-1">
        <div className="driverPic" style={{margin:"0 100px"}}>
          <div className="driverPicture"> {driverPic} </div>
        </div>
        <form className="form-inline" onSubmit={handleSubmit}>
          <div className="price-flex-container-2">
            <div className="price-flex-container-3">
              <div className="mb-3">
                <label for="number" className="form-label">Width</label>
                <input id="widthValidate" onChange={(e) => handleWidthChange(e.target.value)} value={width} name="width" type="number" min="1" className={width >= 0 ? "form-control is-valid" : "form-control is-invalid"} aria-describedby="emailHelp" required />
                <div className="errorMessage">{errorMessage}</div>
              </div>
              <div className="mb-3">
                <label for="quantity" className="form-label">Length</label>
                <input required name="length" type="number" value={length} onChange={(e) => handleLengthChange(e.target.value)} min="1" className={length >= 0 ? "form-control is-valid" : "form-control is-invalid"} />
              </div>
            </div>
            <div className="price-flex-container-4">
              <div className="mb-3">
                <label for="quantity" className="form-label" required="true" >Height</label>
                <input name="height" type="number" value={height} onChange={(e) => handleHeightChange(e.target.value)} min="1" className={height >= 0 ? "form-control is-valid" : "form-control is-invalid"} required />
              </div>
              <div className="mb-3">
                <label for="quantity" className="form-label">Weight</label>
                <input name="weight" type="number" value={weight} aria-describedby="emailHelp" onChange={(e) => handleWeightChange(e.target.value)} min="1" className={weight >= 0 ? "form-control is-valid" : "form-control is-invalid"} required />
              </div>
            </div>
              <label for="quantity" className="form-label-distance">Distance</label>
            <div className="distanceCSS">
              <input name="distance" type="number" value={distance} aria-describedby="emailHelp" onChange={(e) => handleDistanceChange(e.target.value)} min="1" className={distance >= 0 ? "form-control is-valid" : "form-control is-invalid"} required />
            </div>
            < button type="submit" className="btn btn-primary" className="submitButton">Estimate</button>
             {/* <AlertExample></AlertExample> */}
              <form className="form-inline mx-auto">
                <label for="dimension">Dimension:</label>
                <div className="priceCSS">
                  <input type="number" className="total-price" value={result} readonly />   
                </div>
              </form>
              <div id="payment">
              <li><a onClick={redirectPayment} ><span>Go to Payment</span></a></li>
              </div>
               
          </div>
        </form>
      </div>
    </div>
    )
}
