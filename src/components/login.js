import React, {useState,useEffect} from 'react';
import axios from "axios"
import "../css/login.css"
import { useHistory } from 'react-router-dom'


 const  Login = () => {
     const history = useHistory()
    const [user, setUser] = useState({
        email: "", 
        password: "", 
    })
    const [error, setError] = useState(false)


    const [allUser, setAllUser] = useState([])
    
    function isAuth(user, userElement) {
        if(user.email === userElement.email && user.password === userElement.password){
            return true
        }
        else {
            return false 

        }
    }

    useEffect(() => {   
        const fetchUser = async()=>{
            const response = await axios.get("http://localhost:3000/user")
            setAllUser(response.data.allUser)
        }
      
        fetchUser()
    },[]);

    const handleLogin = (e)=>{
        e.preventDefault()
        console.log("object");
        for(let i =0; i<allUser.length; i++){
            if(isAuth(user, allUser[i])){
                history.push("/")
            }
        }

        setError(true)
    }

    const handleChange = (key,value)=>{
        setUser(prevState => ({...prevState, [key]: value}))
        console.log(user);
    }
    return (
    <div>
            <h1 className="header text-center bg-dark header"> Welcome to DJJ Company </h1>   
           <h3 className="body text-center"> Login </h3>

    <form>  
        <div className="container">   
        <h3 style={{display: error ? "block": "none", color: "red"}}>Invalid Email Or Password </h3>

            <label>Email: </label>   
            <input type="text" placeholder="Enter Email" name="username" required onChange={(e)=>handleChange("email", e.target.value)}></input>  
            <label>Password : </label>   
            <input type="password" placeholder="Enter Password" name="password" onChange={(e)=>handleChange("password", e.target.value)} required></input> 
            <button type="submit" onClick={(e)=>handleLogin(e)} >Login</button>   
            <button type="cancel" className="cancelbtn"> Cancel</button>   
             
        </div>  
    </form>
    </div>
    )}

export default Login; 